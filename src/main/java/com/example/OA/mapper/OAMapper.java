package com.example.OA.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface OAMapper {
    public List<HashMap> listPage(@Param("params") Map<String, Object> params);
    public List<HashMap> getUser(@Param("params") Map<String, Object> params);
    public List<HashMap> getUserPageList(@Param("params") Map<String, Object> params);
    public List<HashMap> getPmcDataSql1(@Param("params") Map<String, Object> params);
    public List<HashMap> getPmcDataSql2(@Param("params") Map<String, Object> params);
    public List<HashMap> getPmcDataSql3(@Param("params") Map<String, Object> params);
    public List<HashMap> getPmcDataSql4(@Param("params") Map<String, Object> params);
    public List<HashMap> getPmcPageList(@Param("params") Map<String, Object> params);
    public HashMap getPmcHZ1(@Param("params") Map<String, Object> params);
    public List<HashMap> getPmcJCList();
    public Integer listPageAll(@Param("params") Map<String, Object> params);
    public List uRate_sb_count();

    public List uRate_sb_powerUP(@Param("params") Map<String, Object> params);
}
